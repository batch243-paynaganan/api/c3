const Checkout = require('../models/Checkout')
const { decode } = require('../middleware/auth')
const Product = require('../models/Product')
const Address = require('../models/Address');
const User = require('../models/User');
const Cart = require('../models/Cart');

// checkout via product
const viaProduct = async (req, res) => {
  const userData = decode(req.headers.authorization);
  const productId = req.params.id;
  const { quantity, paymentMethod } = req.body;

  // Find the product and calculate the subtotal
  const product = await Product.findById(productId);
  const subtotal = quantity * product.price;

  // Generate a reference number
  const referenceNumber = Math.random().toString(36).substring(2, 22);

  // Calculate the shipping date
  const shippingDate = new Date();
  shippingDate.setDate(shippingDate.getDate() + 7);

  // Find the user's default shipping address
  const shippingAddress = await Address.findOne({
    user: userData.id,
    isDefault: true
  });

  // Create a new checkout record and save it
  const newCheckout = new Checkout({
    user: userData.id,
    product: productId,
    quantity,
    subtotal,
    paymentMethod,
    referenceNumber,
    shippingDate,
    shippingAddress: shippingAddress._id
  });
  await newCheckout.save();

  // Update the product's stock
  product.stock -= quantity;
  await product.save();

  // Find the user and update their checkout history
  const user = await User.findById(userData.id);
  user.checkoutHistory.push(newCheckout);
  await user.save();

  return res.send(newCheckout);
};

// checkout via cart
const viaCart = async (req,res)=> {
  const userData = decode(req.headers.authorization)
  const cartId = req.params.id;
  const { quantity, paymentMethod } = req.body;

  const cart = await Cart.findById(cartId);
  const productId = cart.product;
  const product = await Product.findById(productId);
  const subtotal = Number(quantity * product.price);

  const referenceNumber = Math.random().toString(36).substring(2,22)

  const shippingDate = new Date()
  shippingDate.setDate(shippingDate.getDate()+7);

  const shippingAddress = await Address.findOne({
    user: userData.id,
    isDefault: true
  })

  const newCheckout = new Checkout({
    user: userData.id,
    cart: cartId,
    quantity,
    subtotal,
    paymentMethod,
    referenceNumber,
    shippingDate,
    shippingAddress: shippingAddress._id
  })
  await newCheckout.save()

  product.stock -= quantity;
  await product.save()

  const user = await User.findById(userData.id)
  user.checkoutHistory.push(newCheckout)
  await user.save()

  cart.quantity -= quantity;
  cart.subtotal = cart.quantity * product.price;

  if(cart.quantity === 0){
    await cart.remove()
  }else{
    await cart.save()
  }
  return res.send(newCheckout)
}



module.exports = {
    viaProduct,
    viaCart
}
