const Review = require('../models/Review')
const Product = require('../models/Product')
const { decode } = require('../middleware/auth')

// create a review
const createReview = async (req, res) => {
  try {
    // Decode user data from JWT
    const userData = decode(req.headers.authorization);
    // Extract productId and review data from request
    const productId = req.params.id;
    const { rating, review } = req.body;

    // Validate input
    if (!productId || !userData || !rating || !review) {
      return res.status(400).send({ message: 'Missing required fields' });
    }
    if (rating < 0 || rating > 5) {
      return res.status(400).send({ message: 'Rating must be between 0 and 5' });
    }
    if (review.length > 200) {
      return res.status(400).send({ message: 'Review must be no more than 200 characters' });
    }

    // Create new Review document
    const newReview = new Review({
      user: userData.id,
      product: productId,
      rating,
      review
    });
    await newReview.save();

    // Update reviewHistory for Product
    const productData = await Product.findById(productId);
    if (!productData) {
      return res.status(404).send({ message: 'Product not found' });
    }
    productData.reviewHistory.push(newReview._id);
    await productData.save();

    res.send({ message: 'Review created successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Error creating review' });
  }
};

// check the reviews
const getReviews = async (req, res) => {
  try {
    // Extract productId from request
    const productId = req.params.id;

    // Find Product document
    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).send({ message: 'Product not found' });
    }

    // Find all Review documents for product
    const reviews = await Review.find({ product: productId }).populate('user');

    if(reviews.length === 0){
        res.status(404).json({mssg:'This product has no reviews yet'})
    }else{
        res.send(reviews)
    }
  } catch (error) {
    console.error(error);
    res.status(500).send({ message: 'Error getting reviews for product' });
  }
};

module.exports = { createReview, getReviews }