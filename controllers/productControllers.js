// dependencies
const Product = require('../models/Product');
const Review = require('../models/Review');

// create a product
const createProduct = (req,res) => {
    const {name, description, image, price, category, size, color, stock} = req.body;
    const product = new Product({
        name,
        description,
        image,
        price,
        category,
        size,
        color,
        stock
    });
    product.save((error)=>{
        if(error){
            console.log(error);
            res.status(500).json({mssg: 'Error creating product'});
        }else{
            res.status(200).json({mssg: 'Product Created Successfully'})
        }
    })
}

// retrieve all products
const getAllProducts = (req,res) => {
    Product.find({},(error,products)=>{
        if(error){
            console.log(error);
            res.status(500).json({mssg:'Error getting products'});
        }else{
            res.status(200).send(products)
        }
    })
}

// retrieves a specific product
const getProduct = (req,res) => {
    Product.findById(req.params.id, (error, product)=>{
        if(error){
            console.log(error);
            res.status(500).json({mssg: 'Error getting product'});
        }else if(product){
            res.send(product)
        }else{
            res.status(404).json({mssg: 'Product not found'})
        }
    })
}

// update a product
const updateProduct = (req,res)=>{
    Product.findByIdAndUpdate(req.params.id,req.body,{new:true},(error,product)=>{
        if(error){
            console.log(error);
            res.status(500).json({mssg: 'Error updating product'})
        }else if(product){
            res.send(product);
        }else{
            res.status(404).send({mssg: 'Product not found'})
        }
    })
}

// delete a product
const deleteProduct = (req, res) => {
  Product.findByIdAndDelete(req.params.id, (error, product) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error deleting product' });
    } else if (product) {
      res.send({ message: 'Product deleted successfully' });
    } else {
      res.status(404).send({ message: 'Product not found' });
    }
  });
};

// update best sellers
const toggleBestSeller = (req, res) => {
  Product.findById(req.params.id, (error, product) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error updating product' });
    } else if (product) {
      product.isBestSeller = !product.isBestSeller;
      product.save((error) => {
        if (error) {
          console.log(error);
          res.status(500).send({ message: 'Error updating product' });
        } else {
          res.send(product);
        }
      });
    } else {
      res.status(404).send({ message: 'Product not found' });
    }
  });
};

// update on sale
const toggleOnSale = (req, res) => {
  Product.findById(req.params.id, (error, product) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error updating product' });
    } else if (product) {
      product.isOnSale = !product.isOnSale;
      product.save((error) => {
        if (error) {
          console.log(error);
          res.status(500).send({ message: 'Error updating product' });
        } else {
          res.send(product);
        }
      });
    } else {
      res.status(404).send({ message: 'Product not found' });
    }
  });
};

// add discount to product or collection
const addDiscount = (req, res) => {
  const { id, category, discount } = req.body;
  let query = {};
  if (id) {
    query._id = id;
  }
  if (category) {
    query.category = category;
  }
  Product.updateMany(query, { $set: { onSaleDiscounted: discount } }, (error, result) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error adding discount' });
    } else {
      res.send({ message: 'Discount added successfully', result });
    }
  });
};

// get all best sellers
const getBestSellers = (req, res) => {
  Product.find({ isBestSeller: true }, (error, products) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error retrieving best sellers' });
    } else {
      res.send(products);
    }
  });
};

// get all on sale
const getOnSale = (req, res) => {
  Product.find({ isOnSale: true }, (error, products) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error retrieving on sale products' });
    } else {
      res.send(products);
    }
  });
};

// get product by category
const getByCategory = (req, res) => {
  const { category } = req.params;
  Product.find({ category }, (error, products) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error retrieving products by category' });
    } else {
      res.send(products);
    }
  });
};

// get by color
const getByColor = (req, res) => {
  const { color } = req.params;
  Product.find({ color }, (error, products) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error retrieving products by color' });
    } else {
      res.send(products);
    }
  });
};

// get by size
const getBySize = (req, res) => {
  const { size } = req.params;
  Product.find({ size }, (error, products) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error retrieving products by size' });
    } else {
      res.send(products);
    }
  });
};

// get by size
const getByPrice = (req, res) => {
  const { price } = req.params;
  Product.find({ price: {$gte: price} }, (error, products) => {
    if (error) {
      console.log(error);
      res.status(500).send({ message: 'Error retrieving products by price' });
    } else {
      res.send(products);
    }
  });
};


  module.exports = {
    createProduct,
    getAllProducts,
    getProduct,
    updateProduct,
    deleteProduct,
    toggleBestSeller,
    toggleOnSale,
    addDiscount,
    getBestSellers,
    getOnSale,
    getByCategory,
    getByColor,
    getBySize,
    getByPrice
  }