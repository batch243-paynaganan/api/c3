// dependencies
require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
const checkoutRoutes = require('./routes/checkoutRoutes')
const addressRoutes = require('./routes/addressRoutes')
const reviewRoutes = require('./routes/reviewRoutes')


// express app
const app=express()

// middlewares
app.use(express())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use((req,res,next)=>{
    console.log(req.path,req.method)
    next()
})

// routes
app.use('/user',userRoutes)
app.use('/products',productRoutes)
app.use('/checkout',checkoutRoutes)
app.use('/address',addressRoutes)
app.use('/review',reviewRoutes)

// connect to db
mongoose.set('strictQuery',true)
mongoose.connect(process.env.MONGO_URI)
.then(()=>{
    // listen for requests
    app.listen(process.env.PORT,()=>{
        console.log(`Now connected to DB & listening to port ${process.env.PORT}`)
    })
})
.catch(err=>console.log(err))