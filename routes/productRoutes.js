// dependencies
const express = require('express')
// controller
const {
    createProduct,
    getAllProducts,
    getProduct,
    updateProduct,
    deleteProduct,
    toggleBestSeller,
    toggleOnSale,
    getBestSellers,
    getOnSale,
    getByCategory,
    getByColor,
    getBySize,
    getByPrice
} = require('../controllers/productControllers')
const router=express.Router()

//no params
router.post('/create',createProduct)
router.get('/all',getAllProducts)
router.get('/bestsellers',getBestSellers)
router.get('/onsale',getOnSale)
// with params
router.patch('/bestsellers/:id',toggleBestSeller)
router.patch('/onsale/:id',toggleOnSale)
router.get('/category/:category', getByCategory)
router.get('/color/:color',getByColor)
router.get('/size/:size',getBySize)
router.get('/price/:price',getByPrice)
router.get('/:id',getProduct)
router.put('/:id',updateProduct)
router.delete('/:id',deleteProduct)

module.exports = router;