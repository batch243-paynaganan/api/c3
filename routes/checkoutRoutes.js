// dependencies
const express = require('express')
// controller
const { viaProduct, viaCart } = require('../controllers/checkoutControllers')
const router=express.Router()

router.post('/cart/:id', viaCart);
router.post('/:id', viaProduct);

module.exports = router;