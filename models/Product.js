const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  size: {
    type: String,
    required: true
  },
  color: {
    type: String,
    required: true
  },
  stock: {
    type: Number,
    required: true,
    default: 0,
    min: 0,
    validate: {
      validator: value => Number.isInteger(value),
      message: 'Stock must be a positive integer'
    }
  },
  isBestSeller: {
    type: Boolean,
    default: false
  },
  isOnSale: {
    type: Boolean,
    default: false
  },
  onSaleDiscounted: {
    type: Number,
    default: 0
  },
  totalRatings:{
    type: Number,
    default: 0
  },
  reviewHistory:[{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Review',
    required: true
  }]
},{timestamps: true});

module.exports = mongoose.model('Product', ProductSchema);