
const mongoose = require('mongoose');

const checkoutSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  cart: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Cart'
  },
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
  },
  shippingAddress: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Address',
    required: true
  },
  quantity: {
    type: Number,
    required: true
  },
  subtotal: {
    type: Number,
    required: true
  },
  paymentMethod: {
    type: String,
    required: true
  },
  referenceNumber: {
    type: String,
    unique: true,  // make the referenceNumber field unique
    maxlength: 50,  // set the maximum length of the referenceNumber field to 50 characters
    required: true
  },
  shippingDate: {
    type: Date,
    default: new Date()
  }
});

module.exports = mongoose.model('Checkout', checkoutSchema);
